<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaylistAudio extends Model
{
    use HasFactory;

    protected $table = 'playlist_audios';

    protected $fillable = [
        'user_id',
        'audio_id',
        'playlist_id',
        'created_at',
        'updated_at'
    ];
}
