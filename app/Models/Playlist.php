<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;

    protected $table = 'playlists';

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'created_at',
        'updated_at'
    ];

    public function songs() {
        return $this->belongsToMany(Audio::class, 'playlist_audios')
            ->orderBy('id', 'desc')
            ->withPivot(['id','audio_id', 'playlist_id', 'user_id', 'created_at', 'updated_at']);
    }
}
