<?php

namespace App\Http\Controllers;

use App\Models\Playlist;
use App\Models\PlaylistAudio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Stripe\StripeClient;

class PlaylistController extends Controller
{
    public function create(Request $request){

        $this->validate($request, [
            'name' => 'required',
        ]);

        Playlist::insert([
            'user_id' => Auth::id(),
            'name' => $request->name,
            'created_at' => Carbon::now()->format('y-m-d'),
            'updated_at' => Carbon::now()->format('y-m-d')
        ]);
    }

    public function getUserPlaylists(){
        $playlists = Playlist::where('user_id', Auth::id())->with('songs')->get();

        return response()->json($playlists);
    }

    public function test(){

        $stripe = new StripeClient(config('app.stripe_secret_key'));

        $user = Auth::user();

        $paymentMethod = $stripe->paymentMethods->create([
            'type' => 'card',
            'card' => [
                'number' => '4242424242424242',
                'exp_month' => 12,
                'exp_year' => 2022,
                'cvc' => '314',
            ],
        ]);

        dd($paymentMethod->id);
    }

    public function getSongsOfSpecificPlaylist($playlistId){
        $playlist = Playlist::where('user_id', Auth::id())
            ->where('id', $playlistId)
            ->with('songs')
            ->get();

        dd($playlist->songs);
    }

    public function getSongs(Request $request){

        $playlist = Playlist::where('id', $request->id)->with('songs')->get();

        $data['songs'] = $playlist[0]->songs;
        $data['name'] = $playlist[0]->name;

        foreach ($data['songs'] as $song) {
            $song->path = Storage::disk('mp3')->url($song->path);
        }

        return response()->json($data);
    }

    public function addToPlaylist($playlistId, $songId){

        PlaylistAudio::insert([
            'user_id' => Auth::id(),
            'audio_id' => $songId,
            'playlist_id' => $playlistId,
            'created_at' => Carbon::now()->format('y-m-d'),
            'updated_at' => Carbon::now()->format('y-m-d')
        ]);

        return response()->json('success');
    }
}
