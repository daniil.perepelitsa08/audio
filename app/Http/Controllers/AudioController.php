<?php

namespace App\Http\Controllers;

use App\Http\Repositories\PlaylistRepository;
use App\Models\Audio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AudioController extends Controller
{
    public function mediaLibraryView(){
        return view('library');
    }

    public function getPlaylist(PlaylistRepository $playlistRepository){

        $playlist = $playlistRepository->getAll(Auth::id());

        $playlist['data'] = array_map(function(array $audio) {
            return [
                'id' => $audio['id'],
                'title' => $audio['title'],
                'path' => Storage::disk('mp3')->url($audio['path']),
                'duration' => $audio['duration'],
                'duration_in_sec' => $audio['duration_in_sec'],
            ];
        },$playlist['data']);

        return response()->json($playlist);
    }

    public function getRecommended(){

        $playlist = Audio::orderBy('id', 'DESC')
            ->paginate(50)
            ->toArray();

        $playlist['data'] = array_map(function(array $audio) {
            return [
                'id' => $audio['id'],
                'title' => $audio['title'],
                'path' => Storage::disk('mp3')->url($audio['path']),
                'duration' => $audio['duration'],
                'duration_in_sec' => $audio['duration_in_sec'],
            ];
        },$playlist['data']);

        return response()->json($playlist);
    }
}
