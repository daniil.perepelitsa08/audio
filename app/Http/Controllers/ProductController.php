<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Stripe\StripeClient;

class ProductController extends Controller
{
    public function show()
    {
        $stripe = new StripeClient(config('app.stripe_secret_key'));
        $product = $stripe->products->retrieve(config('products')[0]);
        $intent = auth()->user()->createSetupIntent();

        return view('show', compact('product', 'intent'));
    }

    public function purchase(Request $request, $product)
    {
        /** @var User $user */
        $user          = auth()->user();
        $paymentMethod = $request->input('payment_method');
        $stripe = new StripeClient(config('app.stripe_secret_key'));

        try {
//            $user->createOrGetStripeCustomer();
//            $user->updateDefaultPaymentMethod($paymentMethod);
            $session = $stripe->checkout->sessions->create([
                'success_url' => back()->getTargetUrl(),
                'cancel_url' => back()->getTargetUrl(),
                'payment_method_types' => ['card'],

                'mode' => 'payment',
                'customer' => $user->stripe_id,
                'line_items' => [[
                    'price' => 'price_1K2DyiA8lUWzBW8TBW3wefcs',
                    'quantity' => 1,
                ]],
                'payment_intent_data' => [
                    'capture_method' => 'automatic'
                ],
                'submit_type' => 'pay',
            ]);

//            $stripe->charges->create([
//                'amount' => 2000,
//                'currency' => 'usd',
//                'customer' => $user->stripe_id,
//            ]);

            return response()->redirectToIntended($session->url);

        } catch (\Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }
    }
}
