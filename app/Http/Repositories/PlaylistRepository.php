<?php

namespace App\Http\Repositories;

use App\Models\Audio;

class PlaylistRepository
{
    public function getAll($userId){
        $playlist = Audio::where('user_id', $userId)
            ->orderBy('id', 'DESC')
            ->paginate(50)
            ->toArray();

        return $playlist;
    }
}
