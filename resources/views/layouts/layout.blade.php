<!doctype html>
<html lang="en" style="width: 100%; height: 100%;">
<head >
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body style="height: 100%; background-color: black">

<div id="app" style="height: 100%; overflow: hidden">
    <header>
        <div class="bg-dark collapse" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">

                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown" style="font-size: 25px">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                        <hr>
{{--                        <h4 class="text-white">Contact:</h4>--}}
{{--                        <ul class="list-unstyled">--}}
{{--                            <li><a href="/library" class="text-white">My media library</a></li>--}}
{{--                            <li><a href="#" class="text-white">About us</a></li>--}}
{{--                            <li><a href="#" class="text-white">Support</a></li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="/" class="navbar-brand d-flex align-items-center">
                    <strong>My Music</strong>
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            </div>
        </div>
    </header>

    @yield('content')
</div>


<script src="{{ mix('js/app.js') }}"></script>

</body>

</html>
