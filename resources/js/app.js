/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue').default;
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';


import VueRouter from 'vue-router';
import MainComponent from "./components/MainComponent";
import Home from "./components/Home";
import Favorites from "./components/Favorites";
import Playlists from "./components/Playlists";
import Top from "./components/Top";
import PlaylistSongs from "./components/PlaylistSongs";
import Search from "./components/Search";

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/favorites',
            name: 'favorites',
            component: Favorites
        },
        {
            path: '/playlists',
            name: 'playlists',
            component: Playlists
        },
        {
            path: '/top',
            name: 'top',
            component: Top
        },
        {
            path: '/playlist/:id/songs',
            name: 'playlist-songs',
            component: PlaylistSongs,
            props: true
        },
        {
            path: '/search',
            name: 'search',
            component: Search
        },
    ]
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    components: { MainComponent },
    router
});
