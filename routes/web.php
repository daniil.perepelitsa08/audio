<?php

use App\Http\Controllers\ArtistController;
use App\Http\Controllers\AudioController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'],
    function (){
        Route::prefix('api')->group(function () {
            Route::get('/play-list', [AudioController::class, 'getPlaylist'])->name('get.playlist');
            Route::get('/recommended', [AudioController::class, 'getRecommended'])->name('get.recommended');
            Route::post('/add-playlist', [PlaylistController::class, 'create'])->name('add.playlist');
            Route::get('/get/play-lists', [PlaylistController::class, 'getUserPlaylists'])->name('get.playlists');
            Route::get('/get/{id}/songs', [PlaylistController::class, 'getSongs']);
            Route::get('/get/{id}/name', [PlaylistController::class, 'getPlaylistName']);
            Route::post('/add-to-playlist/{playlist_id}/song/{song_id}', [PlaylistController::class, 'addToPlaylist']);
            Route::get('/artists', [ArtistController::class, 'index']);
        });
        Route::get('/test', [PlaylistController::class, 'test']);

        Route::post('/products/{id}/purchase', [ProductController::class, 'purchase'])->name('products.purchase');
        Route::get('/show', [ProductController::class, 'show']);

        Route::get('/{any}', [\App\Http\Controllers\SpaController::class, 'index'])->where('any', '.*');
    });
